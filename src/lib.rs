pub mod _cli_core {

use std::{ 
    env, 
    io::BufReader,
    fs::File
};
use std::process::exit;
use xml::reader::{EventReader, XmlEvent};

#[derive(Debug)]
pub struct Project {
    name: String,
    cli: String,
    path: String,
    editor_cmd: String
}
impl Project {
    pub fn new() -> Project {
        Project{name: String::new(), cli: String::new() , path: String::new(), editor_cmd: String::new()}
    } // TODO
    pub fn set_name(&mut self, name: String) {self.name = name}
    pub fn get_name(&self) -> &String {&self.name}
    pub fn set_cli(&mut self, cli: String) {self.cli = cli}
    pub fn get_cli(&self) -> &String {&self.cli}
    pub fn set_path(&mut self, path: String) {self.path = path}
    pub fn get_path(&self) -> &String {&self.path}
    pub fn set_editor_cmd(&mut self, editor_cmd: String) {self.editor_cmd = editor_cmd}
    pub fn get_editor_cmd(&self) -> &String{&self.editor_cmd}

    pub fn from_xml_file() -> Result<Vec<Project>, std::io::Error> {
        let xml_path = match env::var("PROJECT_FILE") {
            Ok(s) => s.replace("~", env::var("HOME").unwrap().as_str()),
            Err(_) => {
                println!("\x1b[1mproject - \x1b[38;5;196mERROR\x1b[37m - Error encountered when attempting to determine project directory from environment variable. Returning.\x1b[0m ");
                exit(1);
                },
        };

        let file = match File::open(xml_path) {
            Ok(v) => v,
            Err(_) => {
                println!("\x1b[1mproject - \x1b[38;5;196mERROR\x1b[37m - Error encountered when attempting to open file specified by \"PROJECT_FILE\". Returning.\x1b[0m ");
                exit(1);
            }
        };
        let file = BufReader::new(file);

        let parser = EventReader::new(file);
        let mut project_list: Vec<Project> = Vec::new();        
        let mut last_open = String::new();
        let mut p = Project::new();
        for event in parser {
            match event {
                Ok(XmlEvent::StartElement {name, .. }) => {
                    last_open = String::from(name.borrow().local_name);
                },
                Ok(XmlEvent::EndElement {name}) => {
                    if String::from(name.borrow().local_name).as_str() == "project" {
                        project_list.push(p);
                        p = Project::new();
                    }
                }
                Ok(XmlEvent::Characters(s)) => {
                    match last_open.as_str() {
                        "name" => p.name = s,
                        "cli" => p.cli = s,
                        "editor-cmd" => p.editor_cmd = s,
                        "path" => p.path = s.replace("~", env::var("HOME").unwrap().as_str()),
                        _ => ()
                    }
                }
                Err(e) => println!("\x1b[1mproject - \x1b[38;5;196mERROR\x1b[37m - Error encountered when attempting to open file specified by \"PROJECT_PATH\". Returning.\x1b[0m\nError Details: {:#?}", e),
                Ok(XmlEvent::EndDocument) => break,
                _ => ()
             }
        }

        return Ok(project_list);
    }
}
}
pub use crate::_cli_core as cli_core;
