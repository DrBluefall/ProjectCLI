
use std::fs;    
use std::process::Command;
use project_cli::cli_core::Project;
fn main() {
    println!("Hello, world!");
    let project_list = match Project::from_xml_file() {
        Ok(v) => v,
        Err(e) => panic!("{:#?}", e),
    };
    println!("{:#?}", project_list);
    let argv: Vec<String> = std::env::args().collect();
    if argv.len() < 2 {
        println!("\x1b[1mproject - \x1b[38;5;196mERROR\x1b[37m - Not enough arguments passed.\x1b[0m");
        std::process::exit(1);
    }
    let cmd = &argv[1];
    for project in project_list {
        if cmd == project.get_cli() {
            Command::new("cd")
                    .arg(project.get_path())
                    .output()
                    .expect("Move to project path failed");
            let mut p = String::from(project.get_path().as_str());
            p.push_str("/.project");
            match fs::metadata(p.as_str()) {
                Ok(_) => {
                    let out = Command::new("sh")
                                    .arg(p)
                                    .output()
                                    .expect("You shouldn't be seeing this...");
                    println!("\x1b[1m.project: stdout stream output:\x1b[0m");
                    println!("{}", match String::from_utf8(out.stdout) {
                        Ok(v) => v,
                        Err(e) => panic!(e)
                    });
                    if out.stderr.len() > 0 {
                    println!("\x1b[1m.project: stderr stream output:\x1b[0m");   
                    println!("{}", match String::from_utf8(out.stderr) {
                        Ok(v) => v,
                        Err(e) => panic!(e)
                    });
                    }
                },
                Err(_) => ()
            };
            Command::new(project.get_editor_cmd())
                    .arg(project.get_path())
                    .output()
                    .expect("Command Failed");
        }
    }
}
